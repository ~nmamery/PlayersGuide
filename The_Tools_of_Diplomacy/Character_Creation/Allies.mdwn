### Allies ###

----

**Ally in the Golden Circle (1)**

: A good friend among the "mundane" folk of either Amber or another
Golden Circle city.

: Please specify details about your Ally; which will need to be
approved.

**Friend in Low Places (1)**

: A good friend among one of the "mundane" folk of Chaos who is in the
area for some reason.  

: Please specify approximately what kind of person; and we will do our
best. 

**Court Friend in either Amber or Chaos (2)**

: A high ranking Amberite or Chaosian has your interests at heart.

: You don't get to specify who, and probably won't find out.

**House Support (3)**

: One of the houses of Chaos supports your character.

: This may or may not be of any use, you don't get to specify who, and
probably won't find out.

**Amber Court Devotee (6)**

: Someone in Amber really really likes you.

: Fear.

: As an added benefit for someone without defined Amberite ancestry
this gets you Amberite ancestry.  

: You don't get to specify who, and probably won't find out.
