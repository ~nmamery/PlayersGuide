### Partial Powers ###

----

Each of the powers is given its own page below.

----
 
* [[Pattern]]
* [[Logrus]]
* [[Trump]]
* [[Shapeshifting]]
* [[Power Words]]
* [[Sorcery]]
* [[Conjuration]]
