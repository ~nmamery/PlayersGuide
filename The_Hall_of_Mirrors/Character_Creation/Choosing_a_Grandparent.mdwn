### Choosing a Grandparent ###

----

*"It is also said that we are descended of Dworkin and the Unicorn, which gave rise to some unusual mental images."*


----
Below is a list of all of the available grandparents for new characters, in no particular order. 

You may also choose not to choose one - in which case the GMs will decide... <br>
You might wish to take an [[Amber Devotee|Allies]] in this case. 

Once we receive everyone's choice of grandparent, we will let you know who your parent is. 

* Random
* Delwin
* Sand
* Dalt
* Fiona
* Brand
* Bleys
* Coral
* Deirdre
* Corwin
* Eric
* Llewella
* Finndo
* Benedict
* Dara
* Flora
* Gerard
* Caine
* Julian
* Sawall
<br>
