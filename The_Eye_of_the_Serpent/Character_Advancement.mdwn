### Character Advancement ###

----
*"Frying pans without fires are often far between"*

----

* 10 additional points can be spent on returning characters.
* No more than 5 points of Good Stuff allowed.
* The powers have still been divided into partial powers.
* There are some things to be aware of when purchasing powers.
* Various kinds of allies are available, but if you want a new ally you will need to suggest what might have brought you to the attention of someone powerful.
* Still no constructs and no Jewel of Judgement attunement!
* There are some FAQs below; more will be added as they come up.
* Please let us know if you still have your Trumps
* A bonus of 1 additional point per hundred words (up to a maximum of 5) is available to any player who sends us a short summary of what happened last time. -- see [[Last Time in Amber...]]

----
**FAQ**

**My character was in an untenable position at the end of the last game they played in. Can I...**
:   **... make a new character?**
:   Yes, and if your character has been involved in every session so far you have 80 points, plus Pattern. If you missed one or more sessions, please check with us.
:   **... play them anyway?**
:   Yes, if you really want to, we'll come up with some way to make it work, but please bear in mind that we can't influence the opinions of the other players/characters!  

<br />
**I'm making a new character; can I have both the 5 extra points for public background and the five extra points for a summary of last time**
: You can have up to 5 extra points in total, but you could produce some background and some summary if you wanted.

<br />
**Can I keep a magical item I picked up on the last adventure?**
:   Yes; you can either pay points for it, in which case it is yours, or not, in which case it will continue to be ours - see the last question below! 

<br />
**Can I do something not covered above?**
:   Yes. 

<br />
**Is it a good idea?**
:   Find out in play!

<br>
