### Shapeshifting ###

----

[[Shapeshifting.png]]

----

**Animal Form (5)**

: The shifter can shift to a specific animal form and back to human at
will.

**Basic Forms (10)**

: The shifter has three specific alternative forms they can shift to
at will: Chaos Form, Avatar Form, and Primal Form.

: Amberite shifters may not know what their Chaos Form is.

: Primal Form involves lost of control of ones actions.

**Automatic Shape Shift (5)**

: The shifter can release control of their shape to their subconsious
in order to survive danger or environmental hazards.

: Without either of the abilities above they can't intentionally shift
back to human form and will have to wait for their subconsious to
appreciate safety.

**Advanced Animal Forms (5)** (Animal Form, Basic Forms)

: The shifter can shift to any animal form they know.  Accuracy of the
form and ability to use it increase with familiarity.

**Partial Shift (5)** (Basic Forms)

: The shifter can shift your body parts or facial features independently of
the rest of their body.  Shifts involving parts of other forms that
are well known are easier to produce and maintain.

**Shift Wounds (5)** (Automatic Shape Shift)

: The shifter heals automatically and swiftly.

**Shift Matter (5)** (Basic Forms)

: The shifter can shapeshift non-living matter.  Only the shape of the matter
changes, not the material.  This includes shifting clothing/armor when
they change form.

**Shift Others (5)** (Shift Matter, Automatic Shape Shift)

: The shifter can shift the form of anyone they have psychic contact
with.  This is slower than personal shifting and can be resisted.

**Shift Mass (5)** (Shift Matter, Automatic Shape Shift)

: A shifter with this power can become lighter or heavier by taking on
or shedding matter from the shadow they are in.  This is quite slow.

: Once the shifter is back in a Basic form they will slowly gain or
lose mass in order to regain their natural mass unless they
concentrate on keeping it.

**Shift Anatomy (10)** (Shift Mass)

: The shifter can change their own internal anatomy and take on
complex abilities of other forms they know (such as the ability to fly
with wings or breath underwater with gills).

: Some of these things can also be achieved with Automatic Shape
Shift, but this power allows them to be done intentionally.

**Shift Aura (5)** (Basic Forms, Automatic Shape Shift)

: This allows the shifter to change the apparent "shape" of their mind
to match that of anything they could change the shape of their body
to.

**Shift Persona (5)** (Shift Aura)

: This allows the shifter to take on the persona of anyone else they
know.

: This risks losing control!

**Create Blood Creatures (5)** (Shift Anatomy)

: This allows you to create independant creatures from your blood.

<br>
