### Notes ###

----

*"To paraphrase Hamlet, Lear and all those other guys: I wish I had known this some time ago."*

----
### General ###
* No constructs
* No Jewel of Judgement attunement ( ! )
* If you have Amberite ancestry you do _not_ know about it.

### Trump ###
* You should not expect to have any trumps at the start of the game.

### Allies ###
* We don't expect Amber Devotee to be worth 6 points.
* Or indeed more than just a Friend in High Places.

### Items ###
* Items will need to be approved.

<br>
